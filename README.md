# Tarea 1 Sistemas Operativos
#### Informacion de los integrantes 
* Mariapaz Gómez 201930043-1

#### Instrucciones de compilación y ejecución 
El prgrama solo debe ser almacenado en una parpeta con todos los archivos dentro del .zip \\
Una vez hecho esto solo se debe situar en la carpeta contenedora y correr la siguiente linea para compilación :
```Shell
    make run 
```
luego para correr el programa :
```Shell
    ./tarea1
```
Finalmente para borrar nuestro ejecutable debemos correr :
```Shell
    make clean
```
#### Suposiciones para la tarea
* **formato del archivo de pedidos :** Asumimos que los pedidos no tienen aignacion para usuario, es decir, no importa el orden de entrega, por lo que el archivo siempre tendra una linea de pedidos.
```txt
    Sandwich , Brownie , Chocolate ,Pepsi ,Agua ,Cafe ,Sandwich ,Pepsi ,Agua ...
```
* **cantidad de pedidos maximo :** Puesto que no se establece si la cantidad de pedidos es limitada o ilimitada lo establecimos hasta un maximo de 100 pedidos.
* **Sumatoria del total :** Asumimos que el total de ventas no es necesario comunicarlo entre cajero y repartidores, por lo que solo lo calculamos con una funcion de suma. 

