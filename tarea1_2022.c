#include<stdio.h>
#include<string.h>
#include <sys/types.h>
#include <unistd.h>

#include "listas.h"   // agrego header de las funciones que utilizo para las listas

int main(){
    //----------------------------------------Proceso padre
    

    // apertura de archivos
    FILE *fp, *fp1;
    char texto[50];
    fp = fopen("Productos.txt", "r"); // apertura de archivos solo en modo lectura
    fp1 = fopen("Pedidos.txt", "r");
    if (fp==NULL)
    {
        printf("El archivo de productos no se puede abrir");
        return -1;
    }
    if (fp1==NULL)
    {
        printf("El archivo de pedidos no se puede abrir");
        return -1;
    }

    // manipulacion de informacion del archivo que contiene los productos 
    listaProductos= (struct Lista*)malloc(sizeof(lista)); // asigno memoria a la lista 
    inicializacion(listaProductos); // inicializo la lista 
    char *nombreProducto = NULL; // variable para 
    int precio=0,codigo=0,tiempo=0;
    while(!feof(fp)){

        fgets(texto,50,fp); // obtengo linea a linea
         // asigno espacio de memoria para estructura
        nombreProducto = strtok(texto," ,"); // obtengo la descripcion del producto
        nombreProducto = strdup(nombreProducto); // asigno memoria especifica ya que es de tipo puntero (char*)
        precio = atoi(strtok(NULL, " ,"));  // obtengo el resto de los elementos y los paso a enteros (int)
        codigo = atoi(strtok(NULL, " ,"));
        tiempo = atoi(strtok(NULL, " ,"));
        nuevoProducto(nombreProducto,precio,codigo,tiempo); // inserto elemento en la lista
    }

    printf("-----------------Lista de productos----------------- \n");
    mostrarProductos();
    fclose(fp);  // cierre de archivo

    int buffer[100], i=0,k;  // lista que guarda los codigos de los productos
    
    for( i = 0; i < 100; i++){buffer[i]=0;}
    
    char listaPedidos[100];
    fgets(listaPedidos,100,fp1);
    producto *productoPedido  = NULL;
    char *pedido = listaPedidos;
    pedido = strtok(listaPedidos," ,");

    i= 0;
    printf("-----------------Lista de pedidos----------------- \n");
    while(pedido!=NULL){

        productoPedido = encontrarProducto(pedido);
        buffer[i] = productoPedido->codigo;
        pedido = strtok(NULL," ,");
        printf("Numero de orden : %d\t| Descripcion : %s\t| precio : %d\n",i, productoPedido->descripcion,productoPedido->precio);
        i++;
    }


    fclose(fp1);
     
    // manejo de procesos hijos (codigo adaptado de ayudantia)
    pid_t pid1,pid2; //Creamos el pid para guardar el retorno del fork
    int tuberia[2];//Creamos la variable para el pipe
    pipe(tuberia); //Creamos el pipe
    pid1 = fork(); //Hacemos fork para crear el proceso hijo 1
    
    if(pid1 < 0){ //Ocurrioun error
        printf("Fallo el fork");
        return 1;
    } 
    else if(pid1 == 0){ //--------------------------------Este es el proceso hijo 1
        printf("Soy el proceso hijo 1, mi PID es %d\n",getpid());
        int buffer[1]; //Variable para lectura
        while(1){
            read(tuberia[0],buffer,sizeof(int)); //Leemos datos del pipe, dos intcada vez
            
            if(buffer[0]==0){ //no existe un codigo 0 por lo que este marca el final del pedido
                break;
            }
            producto *dp1 = encontrarCodigo(buffer[0]); // obtenemos el producto desde su codigo
            int espera1 = dp1->tiempo;
            
            usleep(espera1);
            
            //  entrego el producto luego de esperar el tiempo indicado 
            printf("[hijo1] entrega : %s, tiempo de espera = %d\n",dp1->descripcion,espera1);
        }
        return 0;
    }    

    pid2 = fork(); //Hacemos fork para crear el proceso hijo 2
    if(pid2 < 0){ //Ocurrioun error
        printf("Fallo el fork");
        return 1;
    } 
    else if(pid2 == 0){ //--------------------------------Este es el proceso hijo 2
        printf("Soy el proceso hijo 2, mi PID es %d\n",getpid());
        int buffer[1]; //Variable para lectura
        while(1){
            read(tuberia[0],buffer,sizeof(int)); //Leemos datos del pipe
            i++;
            if(buffer[0]==0){ //no existe un codigo 0 por lo que este marca el final del pedido
                break;
            }
            producto *dp2 =  encontrarCodigo(buffer[0]); // obtenemos el producto desde su codigo
            int espera = 0;
            espera = dp2->tiempo;
           
            usleep(espera); // espero el tiempo que cada producto demora en entregar
            printf("[hijo2] entrega : %s, tiempo de esperado = %d\n",dp2->descripcion,espera);// entrego luego del tiempo esperado
        }
        return 0;
    }

    write(tuberia[1],buffer,sizeof(int)*100); //Escribimos en el pipe los datos
    wait(NULL); //El padre espera a que termine el proceso hijo
    wait(NULL); 
    int sumaVentas = totalVentas(buffer);//cajero suma el buffer de pedidos 
    printf("venta : %d\n", sumaVentas);  
    printf("Soy el padre, mi hijos eran el PID: %d y el PID: %d, yo soy el PID: %d\n",pid1,pid2,getpid());
    return 0;
}

