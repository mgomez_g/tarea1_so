
#include<stdio.h>
#include<string.h>
#include <sys/types.h>
#include <unistd.h>

#include "listas.h"


producto *encontrarProducto(char *descipcion){
    producto *p = listaProductos->inicio, *coincide =  NULL;

    if (p==NULL){
        printf("La lista de productos esta vacía");
    }
    else{
        while (p!=NULL){  
            if(strcmp(p->descripcion,descipcion)==0){ // strcmp retorna 0 si los strings son iguales
                coincide = p;
            }
            p = p->next;
        }
    }
    return coincide;
}

producto *encontrarCodigo(int codigo){
    
    producto *p = listaProductos->inicio, *coincide =  NULL;
    
    if (p==NULL){
        printf("La lista de productos esta vacía");
    }
    else{
        while (p!=NULL){  
            
            if(p->codigo == codigo){ // strcmp retorna 0 si los strings son iguales
                
                coincide = p;
            }
            p = p->next;
        }
    }
    return coincide;
}


void mostrarProductos(){
        producto *aux = NULL;
        aux = listaProductos->inicio;
        while (aux!=NULL){   
            printf("Descripcion : %s\n", aux->descripcion);
            printf("Precio : %d\n", aux->precio);
            printf("Codigo : %d\n", aux->codigo);
            printf("Tiempo : %d\n", aux->tiempo);
            aux= aux->next;
        }
}

void nuevoProducto(char *nombre, int precio,int codigo, int tiempo){
    producto *p = (producto *)malloc(sizeof(producto)); // asigno memoria para nuevo elemento de la lista 
    if(listaProductos->inicio == NULL){ // si la lista esta vacia
        p->descripcion = nombre; // asigno valores a atributos de la estructura 
        p->precio = precio;
        p->codigo = codigo;
        p->tiempo = tiempo;
        p->next = NULL; // el siguiente aun no esta dado, por lo que inicializo en NULL
        listaProductos->inicio = p; // asigno el inicio de la lista como primer elemento 
        listaProductos->size ++; // aumento el tamaño en 1
        listaProductos->final = p; // apunto el final de la lista al primer elemento (podria dejarse en NULL en este caso no genera diferencias)
        header = p; //apunto el header a la cabeza de la lista

        
    } else if(header == NULL){  // si el header no esta asignado correctamente 
        printf("El puntero no contiene una direccion \n");
    } else{   // si ya hay al menos un elemento en la lista
        header->next = p; // apunto el puntero next a el nuevo elemento creado 
        p->descripcion = nombre; // asigno valores a los atributos
        p->precio = precio;
        p->codigo = codigo;
        p->tiempo = tiempo;
        p->next = NULL; // inicializo el puntero next en NULL
        listaProductos->final = p; // apunto al ultimo elemento creado como el ultimo de la lista
        listaProductos->size ++; // aumento el tamaño
        header = p; // asigno el header como el ultimo elemento creado
    }
}

void inicializacion(lista *listaProductos){
    listaProductos->inicio = NULL;
    listaProductos->final = NULL;
    listaProductos->size = 0;
    listaProductos->total = 0;
}

int totalVentas(int listaPedidos[100]){
    int i ,sum = 0;
    producto *p;
    for ( i = 0; i < 99; i++)
    {   
        if(listaPedidos[i] == 0){
            return sum;
            break;
        }
        p = encontrarCodigo(listaPedidos[i]);
        sum = sum + p->precio;
    }
    return sum;
}