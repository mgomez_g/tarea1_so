#ifndef LISTAS_H
#define LISTAS_H

typedef struct Producto {
    char *descripcion;
    int precio,tiempo,codigo;
    struct Producto *next;
    }producto;

typedef struct Lista{
    producto *inicio;
    producto *final;
    int size;
    int total;
}lista;

lista *listaProductos;
producto *header;
void mostrarProductos();
void nuevoProducto(char *nombre, int precio,int codigo, int tiempo);
void inicializacion (lista *header);
producto *encontrarProducto(char *descipcion);
producto *encontrarCodigo(int codigo);
int totalVentas(int listaPedidos[100]);

#endif